package com.common.sqlgenerator.generator;

import com.common.sqlgenerator.annotation.Column;
import com.common.sqlgenerator.annotation.TableGeneration;
import com.google.auto.service.AutoService;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.Messager;
import javax.annotation.processing.Processor;
import javax.annotation.processing.RoundEnvironment;
import javax.annotation.processing.SupportedAnnotationTypes;
import javax.annotation.processing.SupportedSourceVersion;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.TypeElement;
import javax.lang.model.util.Types;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Transient;

@SupportedAnnotationTypes("com.common.sqlgenerator.annotation.TableGeneration")
@SupportedSourceVersion(SourceVersion.RELEASE_17)
@AutoService(Processor.class)
public class TableGenerationProcessor extends AbstractProcessor {

    private final List<TableGenerationWrapper> tableGenerationWrappers = new ArrayList<>();

    @Override
    public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) {
        Messager messager = this.processingEnv.getMessager();
        SqlScriptGenerator sqlScriptGenerator = new SqlScriptGenerator(messager);

        for (Element element : roundEnv.getElementsAnnotatedWith(TableGeneration.class)) {
            var enclosingElements = element.getEnclosedElements();
            var tableGenerationWrapper = new TableGenerationWrapper(element.getAnnotation(TableGeneration.class));
            var typeElement = (TypeElement) element;
            Types typeUtils = this.processingEnv.getTypeUtils();
            Element superElement = typeUtils.asElement(typeElement.getSuperclass());

            // Add all fields from SuperClass
            tableGenerationWrapper.fields()
                                  .addAll(superElement.getEnclosedElements()
                                                      .stream()
                                                      .filter(this::filterField)
                                                      .map(e -> new FieldWrapper(e.getSimpleName().toString(),
                                                              e.asType()
                                                               .toString()
                                                               .substring(e.asType().toString().lastIndexOf('.') + 1),
                                                              e.getAnnotation(Column.class), e.getAnnotation(Id.class),
                                                              e.getAnnotation(Enumerated.class)))
                                                      .toList());

            // Add all fields from Class
            List<FieldWrapper> fields = enclosingElements.stream()
                                                         .filter(this::filterField)
                                                         .map(e -> new FieldWrapper(e.getSimpleName().toString(),
                                                                 e.asType()
                                                                  .toString()
                                                                  .substring(
                                                                          e.asType().toString().lastIndexOf('.') + 1),
                                                                 e.getAnnotation(Column.class),
                                                                 e.getAnnotation(Id.class),
                                                                 e.getAnnotation(Enumerated.class)))
                                                         .toList();
            tableGenerationWrapper.fields().addAll(fields);

            this.tableGenerationWrappers.add(
                    new TableGenerationWrapper(element.getAnnotation(TableGeneration.class), fields));

            //Call Create Table generation
            sqlScriptGenerator.saveCreateTables(tableGenerationWrapper);
        }

        //Create constraints
        sqlScriptGenerator.createConstraints(this.tableGenerationWrappers);

        return true;
    }

    private boolean filterField(Element element) {
        return element.getKind() == ElementKind.FIELD && element.getAnnotation(Transient.class) == null
                && !element.getModifiers().contains(Modifier.PUBLIC);
    }

    record TableGenerationWrapper(TableGeneration tableGeneration, List<FieldWrapper> fields) {
        public TableGenerationWrapper(TableGeneration tableGeneration) {
            this(tableGeneration, new ArrayList<>());
        }

        public TableGenerationWrapper(TableGeneration tableGeneration, List<FieldWrapper> fields) {
            this.tableGeneration = tableGeneration;
            this.fields = fields;
        }
    }

    record FieldWrapper(String name,
                        String type,
                        Column columnAnnotation,
                        Id idAnnotation,
                        Enumerated enumeratedAnnotation) {
    }
}
