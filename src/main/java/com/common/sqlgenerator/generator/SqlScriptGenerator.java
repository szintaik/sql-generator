package com.common.sqlgenerator.generator;

import com.common.sqlgenerator.annotation.Column;
import com.common.sqlgenerator.generator.TableGenerationProcessor.FieldWrapper;
import com.common.sqlgenerator.generator.TableGenerationProcessor.TableGenerationWrapper;
import com.google.common.base.CaseFormat;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import javax.annotation.processing.Messager;
import javax.tools.Diagnostic.Kind;
import org.apache.commons.lang3.StringUtils;

public class SqlScriptGenerator {

    private static final String TABLES_PATH = "src/main/resources/db/migration/tables/";
    private static final String FILE_NAME_PREFIX = "V2_";

    private final Messager messager;

    public SqlScriptGenerator(Messager messager) {
        this.messager = messager;
    }

    public void saveCreateTables(TableGenerationWrapper tableGenerationWrapper) {
        this.generateCreateTable(tableGenerationWrapper);
    }

    public void createConstraints(List<TableGenerationWrapper> tableGenerationWrappers) {
        String fileName = "src/main/resources/db/migration/z_after_schema_init/V3_1_0__constraint.sql";
        Path path = Paths.get(fileName);

        try {
            if (Files.exists(path)) {
                Files.delete(path);
            }
            Files.createFile(path);
        } catch (Exception e) {
            this.messager.printMessage(Kind.ERROR, e.getMessage());
        }

        var query = new StringBuilder();
        for (TableGenerationWrapper clazz : tableGenerationWrappers) {
            var fields = clazz.fields().stream().filter(this::filterFieldsToConstraint).toList();
            var tableName = clazz.tableGeneration().tableName();
            if (!fields.isEmpty()) {
                query.append("-- ").append(tableName).append(" constraints\n");
            }
            for (FieldWrapper field : fields) {
                Column column = field.columnAnnotation();
                String foreignKeyTableName = column.foreignKeyTableName();
                String foreignKeyColumnName = column.foreignKeyColumnName();
                query.append("\nALTER TABLE ")
                     .append(tableName)
                     .append(" ADD CONSTRAINT fk_")
                     .append(tableName)
                     .append("_")
                     .append(this.convertColumName(field.name()))
                     .append("_")
                     .append(foreignKeyTableName)
                     .append("_")
                     .append(foreignKeyColumnName)
                     .append(" FOREIGN KEY(")
                     .append(this.convertColumName(field.name()))
                     .append(") ")
                     .append("REFERENCES ")
                     .append(foreignKeyTableName)
                     .append(" (")
                     .append(foreignKeyColumnName)
                     .append(")");

                if (field.columnAnnotation().isDeleteOnCascade()) {
                    query.append(" ON DELETE CASCADE");
                }

                query.append(";");
            }
            if (!fields.isEmpty()) {
                query.append("\n\n");
            }
        }
        byte[] strToBytes = query.toString().getBytes();
        try {
            Files.write(path, strToBytes, StandardOpenOption.APPEND);
        } catch (Exception e) {
            messager.printMessage(Kind.ERROR, e.getMessage());
        }
    }

    private boolean filterFieldsToConstraint(FieldWrapper fieldWrapper) {
        Column column = fieldWrapper.columnAnnotation();
        return column != null && StringUtils.isNotBlank(column.foreignKeyTableName());
    }

    private List<SqlFileWrapper> loadSqlFileWrappers() {
        File folder = new File("src/main/resources/db/migration/tables");
        List<String> listOfFiles = Arrays.stream(Objects.requireNonNull(folder.listFiles()))
                                         .map(File::getName)
                                         .toList();

        return listOfFiles.stream().map(SqlFileWrapper::new).toList();
    }

    private Integer getNextIndex() {
        return this.loadSqlFileWrappers().stream().mapToInt(SqlFileWrapper::getIndex).max().orElse(0) + 1;
    }

    private String buildFileName(String tableName) {
        return this.loadSqlFileWrappers()
                   .stream()
                   .filter(a -> Objects.equals(a.getTableName(), tableName))
                   .findFirst()
                   .map(a -> TABLES_PATH + a.fileName)
                   .orElse(TABLES_PATH + FILE_NAME_PREFIX + this.getNextIndex() + "_0__" + tableName + ".sql");
    }

    private <T> void generateCreateTable(TableGenerationWrapper tableGenerationWrapper) {
        var fields = tableGenerationWrapper.fields();
        var tableName = tableGenerationWrapper.tableGeneration().tableName();

        var query = new StringBuilder(this.buildSequence(tableName));
        query.append("\nCREATE TABLE ").append(tableName).append("(\n");

        int counter = 0;
        for (FieldWrapper field : fields) {
            String fieldType = field.type().toLowerCase();
            Column columnAnnotation = field.columnAnnotation();

            if (field.idAnnotation() != null) {
                this.buildLine(field, "BIGINT PRIMARY KEY DEFAULT nextval('" + this.buildSequenceName(tableName) + "')",
                        query);
            } else if (this.isColumnAnnotationNotNull(field) && StringUtils.isNotBlank(columnAnnotation.columnType())) {
                this.buildLine(field, columnAnnotation.columnType(), query);
            } else if (Objects.equals(fieldType, "long") || Objects.equals(fieldType, "double") || Objects.equals(
                    fieldType, "integer")) {
                this.buildLine(field, "BIGINT", query);
            } else if (field.enumeratedAnnotation() != null || Objects.equals(fieldType, "string")) {
                this.buildLine(field, "VARCHAR(255)", query);
            } else if (Objects.equals(fieldType, "localdatetime")) {
                this.buildLine(field, "TIMESTAMP", query);
            } else if (Objects.equals(fieldType, "boolean")) {
                this.buildLine(field, "BOOLEAN", query);
            }

            if (counter != fields.size() - 1) {
                query.append(",\n");
            }
            counter++;
        }
        query.append("\n);");

        String fileName = this.buildFileName(tableName);
        Path path = Paths.get(fileName);

        byte[] strToBytes = query.toString().getBytes();

        try {
            Files.write(path, strToBytes);
        } catch (Exception e) {
            this.messager.printMessage(Kind.ERROR, e.getMessage());
        }
    }

    private String buildSequence(String tableName) {
        return "CREATE SEQUENCE IF NOT EXISTS " + this.buildSequenceName(tableName) + "\n" + """
                       INCREMENT BY 1
                       MINVALUE 10000
                       START 10000;
                """;
    }

    private String buildSequenceName(String tableName) {
        return tableName + "_seq";
    }

    private void buildLine(FieldWrapper field, String type, StringBuilder query) {
        query.append("    ")
             .append(this.convertColumName(field.name()))
             .append(this.buildSpacesBeforeType(field.name()))
             .append(type);

        if (this.isColumnAnnotationNotNull(field) && field.columnAnnotation().isUnique()) {
            query.append(" UNIQUE");
        }

        if (this.isColumnAnnotationNotNull(field) && !field.columnAnnotation().isNullable()) {
            query.append(" NOT NULL");
        }
    }

    private boolean isColumnAnnotationNotNull(FieldWrapper fieldWrapper) {
        return fieldWrapper.columnAnnotation() != null;
    }

    private String buildSpacesBeforeType(String fieldName) {
        int maxSpace = 30;
        return " ".repeat(Math.max(0, maxSpace - this.convertColumName(fieldName).length()));
    }

    private String convertColumName(String fieldName) {
        return CaseFormat.UPPER_CAMEL.to(CaseFormat.LOWER_UNDERSCORE, fieldName);
    }

    static class SqlFileWrapper {
        private final String fileName;
        private final String tableName;
        private final Integer index;

        public SqlFileWrapper(String fileName) {
            this.fileName = fileName;
            String tableStr = fileName.substring(fileName.lastIndexOf("__") + 2);
            this.tableName = tableStr.substring(0, tableStr.lastIndexOf('.'));
            String indexStr = fileName.substring(fileName.lastIndexOf("V2_") + 3);
            this.index = Integer.valueOf(indexStr.substring(0, indexStr.indexOf('_')));
        }

        public String getTableName() {
            return this.tableName;
        }

        public Integer getIndex() {
            return this.index;
        }
    }
}

