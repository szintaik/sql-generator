package com.common.sqlgenerator.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.FIELD)
@Retention(RetentionPolicy.SOURCE)
public @interface Column {

    String columnType() default "";

    String foreignKeyTableName() default "";

    String foreignKeyColumnName() default "id";

    boolean isNullable() default true;

    boolean isUnique() default false;

    boolean isDeleteOnCascade() default false;
}
